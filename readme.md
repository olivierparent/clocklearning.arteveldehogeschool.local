Clocklearning app
=================


[TOC]


I. About
--------

This app is made as a proof of concept by [Artevelde University College Ghent][ahs] as part of a project-based scientific research project.

### Project Team

 - Veerle Van Vooren (Lecturer/Researcher, project lead)
 - Olivier Parent    (Lecturer/Researcher, design and development)

II. Installation
----------------

### Local Development Environment

 - Use [Artevelde Laravel Homestead][artestead].

#### Configuration 

    $ artestead edit
 
```
sites:

…

# StartMeUp
# ---------

    - map: www.clocklearning.arteveldehogeschool.local
      to: /home/vagrant/Code/startmeup.arteveldehogeschool.local/www/web
      type: symfony
```

Start with the `provision` option to enable the new configuration option

    $ artestead up --provision

#### Install Dependencies

1. Clone the project and go to the folder.


    $ cd ~/Code/
    $ git clone http://bitbucket.org/olivierparent/clocklearning.arteveldehogeschool.local
    $ cd ~/Code/clocklearning.arteveldehogeschool.local/www/


2. Update Composer


    $ composer self-update


3. Install the Composer packages (`composer.lock`) and then update them


    $ composer install
    $ composer update


4. Login to the Artevelde Laravel Homestead sever via SSH


    $ artestead ssh


5. Copy the default environment settings file.


    vagrant@homestead$ cd ~/Code/clocklearning.arteveldehogeschool.local/www/
    vagrant@homestead$ cp .env.example .env


> Windows:  
> Convert the MS-DOS line endings to Unix line endings so the files can be properly executed. 
>
>      vagrant@homestead$ dos2unix app/artisan


6. Initialize the database (create the database) and then run the Migrations (create database tables) and Seeders (populate the database tables with data)


    vagrant@homestead$ cd ~/Code/clocklearning.arteveldehogeschool.local/www/
    vagrant@homestead$ console artevelde:database:init --seed

### Pages

 - **API**
    - http://www.clocklearning.arteveldehogeschool.local/api/v1/doc
    - http://www.clocklearning.arteveldehogeschool.local/api/v1
 - **Production**
    - http://www.clocklearning.arteveldehogeschool.local
 - **Development**	
    - http://www.clocklearning.arteveldehogeschool.local/app_dev.php

[ahs]:			http://www.arteveldehogeschool.be/en
[artestead]:	https://github.com/OlivierParent/homestead

