# language: en
Feature: User login
  In order to access secured content of the website
  As a user with a certain role
  I need to log in on the login page for that role.

  Background:
    Given the users are:
    |username         |password         |role               |
    |cl_administrator |cl_administrator |ROLE_ADMINISTRATOR |
    |cl_member        |cl_member        |ROLE_MEMBER        |
    |cl_supervisor    |cl_supervisor    |ROLE_SUPERVISOR    |

  @mink:default
  Scenario Outline: log in with valid credentials
    Given I am on the homepage
    When I follow "<role-label>"
      Then I should be on "/en/<role-name>/login"
    When I fill in "<role-name>_login[username]" with "<username>"
      And I fill in "<role-name>_login[password]" with "<password>"
      And I press "Sign on"
    Then I should be on "/en/<role-name>"
      And I should see "<role-label>"

# To see or find the predefined steps in the context (MinkContext), use:
# vagrant@homestead$ bin/behat -di
# vagrant@homestead$ bin/behat -dl

    Examples:
        |username        |password        |role-label   |role-name    |
        |cl_administrator|cl_administrator|Administrator|administrator|
        |cl_member       |cl_member       |Member       |member       |
        |cl_supervisor   |cl_supervisor   |Supervisor   |supervisor   |

  @mink:default
  Scenario Outline: log in with invalid credentials
    Given I am on the homepage
    When I follow "<role-label>"
    Then I should be on "/en/<role-name>/login"
    When I fill in "<role-name>_login[username]" with "<username>"
    And I fill in "<role-name>_login[password]" with "<password>"
    And I press "Sign on"
    Then I should be on "/en/<role-name>/login"
    And I should see "Bad credentials"

    Examples:
      |username|password|role-label   |role-name    |
      |x       |x       |Administrator|administrator|
      |x       |x       |Member       |member       |
      |x       |x       |Supervisor   |supervisor   |

  @mink:default
  Scenario Outline: log in with valid credentials but for different role
    Given I am on the homepage
    When I follow "<role-label>"
    Then I should be on "/en/<role-name>/login"
    When I fill in "<role-name>_login[username]" with "<username>"
    And I fill in "<role-name>_login[password]" with "<password>"
    And I press "Sign on"
    Then I should be on "/en/<role-name>/login"
    And I should see "Bad credentials"

    Examples:
      |username        |password        |role-label   |role-name    |
      |cl_member       |cl_member       |Administrator|administrator|
      |cl_supervisor   |cl_supervisor   |Member       |member       |
      |cl_administrator|cl_administrator|Supervisor   |supervisor   |
