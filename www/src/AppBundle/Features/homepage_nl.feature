# language: nl
Functionaliteit: Homepage
  Om toegang te krijgen tot de beveiligde inhoud van de website
  Als bezoeker
  Moet ik koppelingen naar de aanmeldformulieren te zien krijgen

  @mink:default
  Scenario: Koppelingen op de homepagina
    Gegeven dat ik op de homepage ben
    Als ik naar "/nl" ga
    Dan moet ik de volgende tekst zien "Klokleren met Rika-Tika"
      En moet het element "li:nth-child(1) > a" "Beheerder" bevatten
      En moet het element "li:nth-child(2) > a" "Superviseur" bevatten
      En moet het element "li:nth-child(3) > a" "Lid" bevatten


# To see or find the predefined steps in the context (MinkContext), use:
# vagrant@homestead$ bin/behat -di --lang nl
# vagrant@homestead$ bin/behat -dl --lang nl
