<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Supervisor;
use AppBundle\Traits\ContainerTrait;
use AppBundle\Traits\PasswordTrait;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * LoadSupervisorData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class LoadSupervisorData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    use ContainerTrait, PasswordTrait;

    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            __NAMESPACE__.'\LoadLanguageData',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        // Supervisor
        $supervisor = new Supervisor();
        $em->persist($supervisor); // Manage Entity for persistence.
        $supervisor
            ->setFirstName('Supervisor')
            ->setLastName('Fixture')
            ->setUsername('cl_supervisor')
            ->setPassword('cl_supervisor')
            ->setLanguage($this->getReference('languageNL'))
            ->setProfession(Supervisor::PROFESSION_THERAPIST)
        ;
        $this->hashPassword($supervisor);
        $this->addReference('supervisor', $supervisor); // Reference for the next Data Fixture(s).

        $em->flush(); // Persist all managed objects.
    }
}
