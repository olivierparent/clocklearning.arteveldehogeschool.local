<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Language;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * LoadLanguageData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class LoadLanguageData extends AbstractFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $languageEN = new Language();
        $languageEN->setCode('EN');
        $em->persist($languageEN); // Manage Entity for persistence.
        $this->addReference('languageEN', $languageEN); // Reference for the next Data Fixture(s).

        $languageNL = new Language();
        $languageNL->setCode('NL');
        $em->persist($languageNL); // Manage Entity for persistence.
        $this->addReference('languageNL', $languageNL); // Reference for the next Data Fixture(s).

        $em->flush(); // Persist all managed objects.
    }
}
