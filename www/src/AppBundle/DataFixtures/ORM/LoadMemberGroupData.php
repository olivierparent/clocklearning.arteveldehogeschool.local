<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\MemberGroup;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * LoadMemberGroupData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class LoadMemberGroupData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            __NAMESPACE__.'\LoadSupervisorData',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $memberGroup = new MemberGroup();
        $memberGroup
            ->setName('Default Group')
            ->setSupervisor($this->getReference('supervisor'))
        ;
        $em->persist($memberGroup); // Manage Entity for persistence.
        $this->addReference('memberGroup', $memberGroup); // Reference for the next Data Fixture(s).

        $em->flush(); // Persist all managed objects.
    }
}
