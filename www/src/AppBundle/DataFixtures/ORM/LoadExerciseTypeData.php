<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\ExerciseType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * LoadExerciseTypeData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class LoadExerciseTypeData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            __NAMESPACE__.'\LoadModuleData',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $exerciseType01 = new ExerciseType();
        $exerciseType01
            ->setName('Recognizing: analogue clock → 4 notations')
            ->setIsDigital(false)
            ->setIsActive(true)
            ->setModule($this->getReference('moduleB'))
        ;
        $em->persist($exerciseType01); // Manage Entity for persistence.
        $this->addReference('exerciseType01', $exerciseType01); // Reference for the next Data Fixture(s).

        $exerciseType02 = new ExerciseType();
        $exerciseType02
            ->setName('Recognizing: digital clock → 4 notations')
            ->setIsDigital(true)
            ->setIsActive(true)
            ->setModule($this->getReference('moduleB'))
        ;
        $em->persist($exerciseType02); // Manage Entity for persistence.
        $this->addReference('exerciseType02', $exerciseType02); // Reference for the next Data Fixture(s).

        $exerciseType03 = new ExerciseType();
        $exerciseType03
            ->setName('Recognizing: notation → 4 analogue clocks')
            ->setIsDigital(false)
            ->setIsActive(true)
            ->setModule($this->getReference('moduleB'))
        ;
        $em->persist($exerciseType03); // Manage Entity for persistence.
        $this->addReference('exerciseType03', $exerciseType03); // Reference for the next Data Fixture(s).

        $exerciseType04 = new ExerciseType();
        $exerciseType04
            ->setName('Recognizing: notation → 4 digital clocks')
            ->setIsDigital(true)
            ->setIsActive(true)
            ->setModule($this->getReference('moduleB'))
        ;
        $em->persist($exerciseType04); // Manage Entity for persistence.
        $this->addReference('exerciseType04', $exerciseType04); // Reference for the next Data Fixture(s).

        $exerciseType05 = new ExerciseType();
        $exerciseType05
            ->setName('Combining: 4 notations → 4 analogue clocks')
            ->setIsDigital(false)
            ->setIsActive(true)
            ->setModule($this->getReference('moduleB'))
        ;
        $em->persist($exerciseType05); // Manage Entity for persistence.
        $this->addReference('exerciseType05', $exerciseType05); // Reference for the next Data Fixture(s).

        $exerciseType06 = new ExerciseType();
        $exerciseType06
            ->setName('Combining: 4 notations → 4 digital clocks')
            ->setIsDigital(true)
            ->setIsActive(true)
            ->setModule($this->getReference('moduleB'))
        ;
        $em->persist($exerciseType06); // Manage Entity for persistence.
        $this->addReference('exerciseType06', $exerciseType06); // Reference for the next Data Fixture(s).

        $exerciseType07 = new ExerciseType();
        $exerciseType07
            ->setName('Time notation: analogue clock → fill in notation')
            ->setIsDigital(false)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleB'))
        ;
        $em->persist($exerciseType07); // Manage Entity for persistence.
        $this->addReference('exerciseType07', $exerciseType07); // Reference for the next Data Fixture(s).

        $exerciseType08 = new ExerciseType();
        $exerciseType08
            ->setName('Time notation: digital clock → fill in notation')
            ->setIsDigital(true)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleB'))
        ;
        $em->persist($exerciseType08); // Manage Entity for persistence.
        $this->addReference('exerciseType08', $exerciseType08); // Reference for the next Data Fixture(s).

        $exerciseType09 = new ExerciseType();
        $exerciseType09
            ->setName('Time notation: analogue clock → assignment, fill in notation')
            ->setIsDigital(false)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleB'))
        ;
        $em->persist($exerciseType09); // Manage Entity for persistence.
        $this->addReference('exerciseType09', $exerciseType09); // Reference for the next Data Fixture(s).

        $exerciseType10 = new ExerciseType();
        $exerciseType10
            ->setName('Time notation: digital clock → assignment, fill in notation')
            ->setIsDigital(true)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleB'))
        ;
        $em->persist($exerciseType10); // Manage Entity for persistence.
        $this->addReference('exerciseType10', $exerciseType10); // Reference for the next Data Fixture(s).

        $exerciseType11 = new ExerciseType();
        $exerciseType11
            ->setName('Clock setting: notation → set analogue clock')
            ->setIsDigital(false)
            ->setIsActive(true)
            ->setModule($this->getReference('moduleC'))
        ;
        $em->persist($exerciseType11); // Manage Entity for persistence.
        $this->addReference('exerciseType11', $exerciseType11); // Reference for the next Data Fixture(s).

        $exerciseType12 = new ExerciseType();
        $exerciseType12
            ->setName('Clock setting: notation → set digital clock')
            ->setIsDigital(true)
            ->setIsActive(true)
            ->setModule($this->getReference('moduleC'))
        ;
        $em->persist($exerciseType12); // Manage Entity for persistence.
        $this->addReference('exerciseType12', $exerciseType12); // Reference for the next Data Fixture(s).

        $exerciseType13 = new ExerciseType();
        $exerciseType13
            ->setName('Clock setting: notation → assignment, set analogue clock')
            ->setIsDigital(false)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleC'))
        ;
        $em->persist($exerciseType13); // Manage Entity for persistence.
        $this->addReference('exerciseType13', $exerciseType13); // Reference for the next Data Fixture(s).

        $exerciseType14 = new ExerciseType();
        $exerciseType14
            ->setName('Clock setting: notation → assignment, set digital clock')
            ->setIsDigital(true)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleC'))
        ;
        $em->persist($exerciseType14); // Manage Entity for persistence.
        $this->addReference('exerciseType14', $exerciseType14); // Reference for the next Data Fixture(s).

        $exerciseType15 = new ExerciseType();
        $exerciseType15
            ->setName('Recognizing: analogue clock → 4 digital clocks')
            ->setIsDigital(false)
            ->setIsActive(true)
            ->setModule($this->getReference('moduleD'))
        ;
        $em->persist($exerciseType15); // Manage Entity for persistence.
        $this->addReference('exerciseType15', $exerciseType15); // Reference for the next Data Fixture(s).

        $exerciseType16 = new ExerciseType();
        $exerciseType16
            ->setName('Recognizing: digital clock → 4 analogue clocks')
            ->setIsDigital(true)
            ->setIsActive(true)
            ->setModule($this->getReference('moduleD'))
        ;
        $em->persist($exerciseType16); // Manage Entity for persistence.
        $this->addReference('exerciseType16', $exerciseType16); // Reference for the next Data Fixture(s).

        $exerciseType17 = new ExerciseType();
        $exerciseType17
            ->setName('Combining: 4 analogue clocks → 4 digital clocks')
            ->setIsDigital(false)
            ->setIsActive(true)
            ->setModule($this->getReference('moduleD'))
        ;
        $em->persist($exerciseType17); // Manage Entity for persistence.
        $this->addReference('exerciseType17', $exerciseType17); // Reference for the next Data Fixture(s).

        $exerciseType18 = new ExerciseType();
        $exerciseType18
            ->setName('Clock setting: analogue clock → set digital clock')
            ->setIsDigital(false)
            ->setIsActive(true)
            ->setModule($this->getReference('moduleD'))
        ;
        $em->persist($exerciseType18); // Manage Entity for persistence.
        $this->addReference('exerciseType18', $exerciseType18); // Reference for the next Data Fixture(s).

        $exerciseType19 = new ExerciseType();
        $exerciseType19
            ->setName('Clock setting: digital clock → set analogue clock')
            ->setIsDigital(true)
            ->setIsActive(true)
            ->setModule($this->getReference('moduleD'))
        ;
        $em->persist($exerciseType19); // Manage Entity for persistence.
        $this->addReference('exerciseType19', $exerciseType19); // Reference for the next Data Fixture(s).

        $exerciseType20 = new ExerciseType();
        $exerciseType20
            ->setName('Clock setting: analogue clock → set 2 digital clocks (12/24 hour)')
            ->setIsDigital(false)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleD'))
        ;
        $em->persist($exerciseType20); // Manage Entity for persistence.
        $this->addReference('exerciseType20', $exerciseType20); // Reference for the next Data Fixture(s).

        $exerciseType21 = new ExerciseType();
        $exerciseType21
            ->setName('Time notation: 2 analogue clocks → fill in 2 notations, assignment')
            ->setIsDigital(false)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleE'))
        ;
        $em->persist($exerciseType21); // Manage Entity for persistence.
        $this->addReference('exerciseType21', $exerciseType21); // Reference for the next Data Fixture(s).

        $exerciseType22 = new ExerciseType();
        $exerciseType22
            ->setName('Time notation: 2 digital clocks → fill in 2 notations, assignment')
            ->setIsDigital(true)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleE'))
        ;
        $em->persist($exerciseType22); // Manage Entity for persistence.
        $this->addReference('exerciseType22', $exerciseType22); // Reference for the next Data Fixture(s).

        $exerciseType23 = new ExerciseType();
        $exerciseType23
            ->setName('Clock setting: analogue clock → assignment, set analogue clock later')
            ->setIsDigital(false)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleE'))
        ;
        $em->persist($exerciseType23); // Manage Entity for persistence.
        $this->addReference('exerciseType23', $exerciseType23); // Reference for the next Data Fixture(s).

        $exerciseType24 = new ExerciseType();
        $exerciseType24
            ->setName('Clock setting: digital clock → assignment, set digtal clock later')
            ->setIsDigital(true)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleE'))
        ;
        $em->persist($exerciseType24); // Manage Entity for persistence.
        $this->addReference('exerciseType24', $exerciseType24); // Reference for the next Data Fixture(s).

        $exerciseType25 = new ExerciseType();
        $exerciseType25
            ->setName('Order: 2 analogue clocks → which clock shows earlier?')
            ->setIsDigital(false)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleE'))
        ;
        $em->persist($exerciseType25); // Manage Entity for persistence.
        $this->addReference('exerciseType25', $exerciseType25); // Reference for the next Data Fixture(s).

        $exerciseType26 = new ExerciseType();
        $exerciseType26
            ->setName('Order: 2 digital clocks → which clock shows earlier?')
            ->setIsDigital(true)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleE'))
        ;
        $em->persist($exerciseType26); // Manage Entity for persistence.
        $this->addReference('exerciseType26', $exerciseType26); // Reference for the next Data Fixture(s).

        $exerciseType27 = new ExerciseType();
        $exerciseType27
            ->setName('Order: 2 analogue clocks → which clock shows later?')
            ->setIsDigital(false)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleE'))
        ;
        $em->persist($exerciseType27); // Manage Entity for persistence.
        $this->addReference('exerciseType27', $exerciseType27); // Reference for the next Data Fixture(s).

        $exerciseType28 = new ExerciseType();
        $exerciseType28
            ->setName('Order: 2 digital clocks → which clock shows later?')
            ->setIsDigital(true)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleE'))
        ;
        $em->persist($exerciseType28); // Manage Entity for persistence.
        $this->addReference('exerciseType28', $exerciseType28); // Reference for the next Data Fixture(s).

        $exerciseType29 = new ExerciseType();
        $exerciseType29
            ->setName('Order: 4 analogue clocks → order clocks from early to late')
            ->setIsDigital(false)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleE'))
        ;
        $em->persist($exerciseType29); // Manage Entity for persistence.
        $this->addReference('exerciseType29', $exerciseType29); // Reference for the next Data Fixture(s).

        $exerciseType30 = new ExerciseType();
        $exerciseType30
            ->setName('Order: 4 digital clocks → order clocks from early to late')
            ->setIsDigital(true)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleE'))
        ;
        $em->persist($exerciseType30); // Manage Entity for persistence.
        $this->addReference('exerciseType30', $exerciseType30); // Reference for the next Data Fixture(s).

        $exerciseType31 = new ExerciseType();
        $exerciseType31
            ->setName('Order: 4 analogue clocks → order clocks from late to early')
            ->setIsDigital(false)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleE'))
        ;
        $em->persist($exerciseType31); // Manage Entity for persistence.
        $this->addReference('exerciseType31', $exerciseType31); // Reference for the next Data Fixture(s).

        $exerciseType32 = new ExerciseType();
        $exerciseType32
            ->setName('Order: 4 digital clocks → order clocks from late to early')
            ->setIsDigital(true)
            ->setIsActive(false)
            ->setModule($this->getReference('moduleE'))
        ;
        $em->persist($exerciseType32); // Manage Entity for persistence.
        $this->addReference('exerciseType32', $exerciseType32); // Reference for the next Data Fixture(s).

        $em->flush(); // Persist all managed objects.
    }
}
