<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Member;
use AppBundle\Traits\ContainerTrait;
use AppBundle\Traits\PasswordTrait;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * LoadMemberData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class LoadMemberData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    use ContainerTrait, PasswordTrait;

    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            __NAMESPACE__.'\LoadLanguageData',
            __NAMESPACE__.'\LoadMemberGroupData',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $locale = 'nl_BE';
        $faker = Faker::create($locale);

        $member = new Member();
        $em->persist($member); // Manage Entity for persistence.
        $member
            ->setFirstName('Member')
            ->setLastName('Fixture')
            ->setUsername('cl_member')
            ->setPassword('cl_member')
            ->setLanguage($this->getReference('languageNL'))
            ->setMemberGroup($this->getReference('memberGroup'))
            ->setBirthday($faker->dateTime($max = '-6 years'))
        ;
        $this->hashPassword($member);
        $this->addReference('member', $member); // Reference for the next Data Fixture(s).

        for ($i = 0; $i < 10; ++$i) {
            $member = new Member();
            $em->persist($member); // Manage Entity for persistence.
            $member
                ->setFirstName($faker->firstName)
                ->setLastName($faker->lastName)
                ->setUsername($faker->userName)
                ->setPassword($faker->password())
                ->setLanguage($this->getReference('languageNL'))
                ->setMemberGroup($this->getReference('memberGroup'))
                ->setBirthday($faker->dateTime($max = '-6 years'))
            ;
            $this->hashPassword($member);
        }

        $em->flush(); // Persist all managed objects.
    }
}
