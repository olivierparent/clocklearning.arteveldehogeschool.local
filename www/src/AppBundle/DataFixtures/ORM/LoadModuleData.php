<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Module;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * LoadModuleData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class LoadModuleData extends AbstractFixture
{
    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        $moduleA = new Module();
        $moduleA
            ->setName('Module clock knowledge')
            ->setIsActive(false)
        ;
        $em->persist($moduleA); // Manage Entity for persistence.
        $this->addReference('moduleA', $moduleA); // Reference for the next Data Fixture(s).

        $moduleB = new Module();
        $moduleB
            ->setName('Module clock reading')
            ->setIsActive(true)
        ;
        $em->persist($moduleB); // Manage Entity for persistence.
        $this->addReference('moduleB', $moduleB); // Reference for the next Data Fixture(s).

        $moduleC = new Module();
        $moduleC
            ->setName('Module setting time')
            ->setIsActive(true)
        ;
        $em->persist($moduleC); // Manage Entity for persistence.
        $this->addReference('moduleC', $moduleC); // Reference for the next Data Fixture(s).

        $moduleD = new Module();
        $moduleD
            ->setName('Module transposing clocks')
            ->setIsActive(true)
        ;
        $em->persist($moduleD); // Manage Entity for persistence.
        $this->addReference('moduleD', $moduleD); // Reference for the next Data Fixture(s).

        $moduleE = new Module();
        $moduleE
            ->setName('Module interpreting clocks')
            ->setIsActive(false)
        ;
        $em->persist($moduleE); // Manage Entity for persistence.
        $this->addReference('moduleE', $moduleE); // Reference for the next Data Fixture(s).

        $em->flush(); // Persist all managed objects.
    }
}
