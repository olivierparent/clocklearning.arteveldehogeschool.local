<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Administrator;
use AppBundle\Traits\ContainerTrait;
use AppBundle\Traits\PasswordTrait;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * LoadAdministratorData.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class LoadAdministratorData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface
{
    use ContainerTrait, PasswordTrait;

    /**
     * {@inheritdoc}
     */
    public function getDependencies()
    {
        return [
            __NAMESPACE__.'\LoadLanguageData',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $em)
    {
        // Administrator
        $administrator = new Administrator();
        $em->persist($administrator); // Manage Entity for persistence.
        $administrator
            ->setFirstName('Administrator')
            ->setLastName('Fixture')
            ->setUsername('cl_administrator')
            ->setPassword('cl_administrator')
            ->setLanguage($this->getReference('languageNL'))
            ->setEmail('administrator@arteveldehs.be')
        ;
        $this->hashPassword($administrator);
        $this->addReference('administrator', $administrator); // Reference for the next Data Fixture(s).

        $em->flush(); // Persist all managed objects.
    }
}
