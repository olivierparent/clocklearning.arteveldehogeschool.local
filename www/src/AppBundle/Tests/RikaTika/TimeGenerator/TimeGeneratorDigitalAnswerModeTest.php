<?php

#
# $ phpunit -c app/ src/AppBundle/Tests/RikaTika/TimeGenerator/TimeGeneratorDigitalAnswerModeTest.php
#


namespace AppBundle\Tests\RikaTika\TimeGenerator;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * TimeGeneratorDigitalAnswerModeTest.
 *
 * @see http://phpunit.de/manual/current/en/appendixes.assertions.html
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class TimeGeneratorDigitalAnswerModeTest extends WebTestCase
{
    /**
     *
     */
    public function testErrorDigitalEarlierBy30M()
    {
    }

    /**
     *
     */
    public function testErrorDigitalEarlierBy60M()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadH×10()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadH×100()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadHour()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadM00As0()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadReverseMDigits()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadQuartersAsNumber()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadSwapHAndM()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadSwapHAndMThenPast()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadSwapHAndMThenReverseHDigits()
    {
    }

    /**
     *
     */
    public function testErrorDigitalReadTo()
    {
    }

    /**
     *
     */
    public function testErrorDigitalSwapPastAndTo()
    {
    }

    /**
     *
     */
    public function testErrorDigitalSwapPastAndToThenReadQuartersAsNumber()
    {
    }
}
