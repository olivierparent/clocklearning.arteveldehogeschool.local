<?php

#
# $ phpunit -c app/ src/AppBundle/Tests/RikaTika/TimeGenerator/TimeGeneratorAnalogueAnswerModeTest.php
#


namespace AppBundle\Tests\RikaTika\TimeGenerator;

use AppBundle\RikaTika\TimeGenerator\TimeGeneratorAbstract;
use AppBundle\RikaTika\TimeGenerator\TimeGeneratorNL;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * TimeGeneratorAnalogueAnswerModeTest.
 *
 * @see http://phpunit.de/manual/current/en/appendixes.assertions.html
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class TimeGeneratorAnalogueAnswerModeTest extends WebTestCase
{
    /**
     *
     */
    public function testErrorAnalogueH0012M00()
    {
        TimeGeneratorAbstract::setModuleSettings();

        // 24-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $time->setTime(11, 11);
        $this->assertSame('11:11', $time->toTime());

        $time->errorAnalogueH0012M00();
        $this->assertSame(TimeGeneratorAbstract::ERROR_ANALOGUE_H_00_12_M_00, $time->getAnswerMode());
        $this->assertSame('00:00', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(13, 13);
        $this->assertEquals('13:13', $time->toTime());

        $time->errorAnalogueH0012M00();
        $this->assertSame(TimeGeneratorAbstract::ERROR_ANALOGUE_H_00_12_M_00, $time->getAnswerMode());
        $this->assertSame('12:00', $time->toTime());

        // 12-hour clock
        TimeGeneratorNL::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $time->setTime(11, 11);
        $this->assertSame('11:11', $time->toTime());

        $time->errorAnalogueH0012M00();
        $this->assertSame(TimeGeneratorAbstract::ERROR_ANALOGUE_H_00_12_M_00, $time->getAnswerMode());
        $this->assertSame('12:00', $time->toTime());
    }

    /**
     *
     */
    public function testErrorAnalogueH0315M15()
    {
        TimeGeneratorAbstract::setModuleSettings();

        // 24-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $time->setTime(11, 11);
        $this->assertSame('11:11', $time->toTime());

        $time->errorAnalogueH0315M15();
        $this->assertSame(TimeGeneratorAbstract::ERROR_ANALOGUE_H_03_15_M_15, $time->getAnswerMode());
        $this->assertSame('03:15', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(13, 13);
        $this->assertEquals('13:13', $time->toTime());

        $time->errorAnalogueH0315M15();
        $this->assertSame(TimeGeneratorAbstract::ERROR_ANALOGUE_H_03_15_M_15, $time->getAnswerMode());
        $this->assertSame('15:15', $time->toTime());

        // 12-hour clock
        TimeGeneratorNL::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse(TimeGeneratorNL::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $time->setTime(11, 11);
        $this->assertSame('11:11', $time->toTime());

        $time->errorAnalogueH0315M15();
        $this->assertSame(TimeGeneratorAbstract::ERROR_ANALOGUE_H_03_15_M_15, $time->getAnswerMode());
        $this->assertSame('03:15', $time->toTime());
    }

    /**
     *
     */
    public function testErrorAnalogueH0517M30()
    {
        TimeGeneratorAbstract::setModuleSettings();

        // 24-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $time->setTime(11, 11);
        $this->assertSame('11:11', $time->toTime());

        $time->errorAnalogueH0517M30();
        $this->assertSame(TimeGeneratorAbstract::ERROR_ANALOGUE_H_05_17_M_30, $time->getAnswerMode());
        $this->assertSame('05:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(13, 13);
        $this->assertEquals('13:13', $time->toTime());

        $time->errorAnalogueH0517M30();
        $this->assertSame(TimeGeneratorAbstract::ERROR_ANALOGUE_H_05_17_M_30, $time->getAnswerMode());
        $this->assertSame('17:30', $time->toTime());

        // 12-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $time->setTime(11, 11);
        $this->assertSame('11:11', $time->toTime());

        $time->errorAnalogueH0517M30();
        $this->assertSame(TimeGeneratorAbstract::ERROR_ANALOGUE_H_05_17_M_30, $time->getAnswerMode());
        $this->assertSame('05:30', $time->toTime());
    }

    /**
     *
     */
    public function testErrorAnalogueH0921M45()
    {
        TimeGeneratorAbstract::setModuleSettings();

        // 24-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $time->setTime(11, 11);
        $this->assertSame('11:11', $time->toTime());

        $time->errorAnalogueH0921M45();
        $this->assertSame(TimeGeneratorAbstract::ERROR_ANALOGUE_H_09_21_M_45, $time->getAnswerMode());
        $this->assertSame('09:45', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(13, 13);
        $this->assertEquals('13:13', $time->toTime());

        $time->errorAnalogueH0921M45();
        $this->assertSame(TimeGeneratorAbstract::ERROR_ANALOGUE_H_09_21_M_45, $time->getAnswerMode());
        $this->assertSame('21:45', $time->toTime());

        // 12-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $time->setTime(11, 11);
        $this->assertSame('11:11', $time->toTime());

        $time->errorAnalogueH0921M45();
        $this->assertSame(TimeGeneratorAbstract::ERROR_ANALOGUE_H_09_21_M_45, $time->getAnswerMode());
        $this->assertSame('09:45', $time->toTime());
    }

    /**
     *
     */
    public function testErrorAnalogueHoursHalfHourEarlier()
    {
        TimeGeneratorAbstract::setModuleSettings();

        // 24-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourEarlier();
        $this->assertSame('23:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(1);
        $this->assertSame('01:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourEarlier();
        $this->assertSame('00:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(11);
        $this->assertSame('11:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourEarlier();
        $this->assertSame('10:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(12);
        $this->assertSame('12:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourEarlier();
        $this->assertSame('11:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(13);
        $this->assertSame('13:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourEarlier();
        $this->assertSame('12:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(23);
        $this->assertSame('23:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourEarlier();
        $this->assertSame('22:30', $time->toTime());

        // 12-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourEarlier();
        $this->assertSame('11:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(1);
        $this->assertSame('01:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourEarlier();
        $this->assertSame('12:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(11);
        $this->assertSame('11:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourEarlier();
        $this->assertSame('10:30', $time->toTime());
    }

    /**
     *
     */
    public function testErrorAnalogueHoursHalfHourLater()
    {
        TimeGeneratorAbstract::setModuleSettings();

        // 24-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $this->assertSame('00:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourLater();
        $this->assertSame('00:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(1);
        $this->assertSame('01:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourLater();
        $this->assertSame('01:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(11);
        $this->assertSame('11:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourLater();
        $this->assertSame('11:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(12);
        $this->assertSame('12:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourLater();
        $this->assertSame('12:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(13);
        $this->assertSame('13:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourLater();
        $this->assertSame('13:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(23);
        $this->assertSame('23:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourLater();
        $this->assertSame('23:30', $time->toTime());

        // 12-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(false);
        $this->assertFalse(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

        $time = new TimeGeneratorNL();
        $this->assertSame('12:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourLater();
        $this->assertSame('12:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(1);
        $this->assertSame('01:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourLater();
        $this->assertSame('01:30', $time->toTime());

        $time = new TimeGeneratorNL();
        $time->setTime(11);
        $this->assertSame('11:00', $time->toTime());
        $time->errorAnalogueHoursHalfHourLater();
        $this->assertSame('11:30', $time->toTime());
    }

    /**
     *
     */
    public function testErrorAnalogueHoursHourEarlier()
    {
        TimeGeneratorAbstract::setModuleSettings();

        // 24-hour clock
        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(true);
        $this->assertTrue(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());

//        $time = new TimeGeneratorNL();
//        $time->setTime();
//        $this->assertSame('00:00', $time->toTime());
//
//        $time->errorAnalogueHoursHourEarlier();
//        $this->assertSame('23:00', $time->toTime());
//
//
//
//        $time = new TimeGeneratorNL();
//        $time->setTime(12);
//        $this->assertSame('12:00', $time->toTime());
//
//
//
//        $time->errorAnalogueHoursHalfHourLater();
//        $this->assertSame('00:30', $time->toTime());
//
//        // 12-hour clock
//        TimeGeneratorAbstract::getModuleSettings()->setHasTwentyFour(false);
//        $this->assertFalse(TimeGeneratorAbstract::getModuleSettings()->getHasTwentyFour());
//
//        $time = new TimeGeneratorNL();
//        $time->setTime();
//        $this->assertSame('12:00', $time->toTime());
//
//        $time->errorAnalogueHoursHalfHourLater();
//        $this->assertSame('12:30', $time->toTime());
    }

    /**
     *
     */
    public function testErrorAnalogueHoursHourLater()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueHoursMirrorY()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueHoursMirrorZ()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueMinutes01mEarlier()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueMinutes01mDifference()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueMinutes01mLater()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueMinutes05mEarlier()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueMinutes05mDifference()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueMinutes05mLater()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueMinutesMirrorY()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueMinutesMirrorZ()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueMinutesReadAsAdditionToHours()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueMinutesReadAsHours()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueMinutesReadAsHoursWithPastTo()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueReadAsFullHours()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueSwapHands()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueSwapHandsNearestHour()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueSwapHandsReadAsFullHours()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueSwapHandsReadAsHours()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueSwapHandsReadAsHoursWithPastTo()
    {
    }

    /**
     *
     */
    public function testErrorAnalogueSwapHandsReadAsMinutesWithPastTo()
    {
    }
}
