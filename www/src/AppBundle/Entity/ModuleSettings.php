<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ModuleSettings.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 *
 * @ORM\Table(name="module_settings")
 * @ORM\Entity
 */
class ModuleSettings
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_twenty_four", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasTwentyFour = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_quadrants", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasQuadrants = true; // TRUE: Official clock system, FALSE: Alternative clock sytem.

    /**
     * @var bool
     *
     * @ORM\Column(name="has_sound", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasSound = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_mascot", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasMascot = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_colours", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasColours = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_colours_switched", type="boolean", nullable=false, options={"default" = false})
     */
    private $hasColoursSwitched = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_analogue_hours", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasAnalogueHours = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_analogue_minutes", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasAnalogueMinutes = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_analogue_dial_numbers", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasAnalogueDialNumbers = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_analogue_colours", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasAnalogueColours = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_digital_hours", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasDigitalHours = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_digital_minutes", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasDigitalMinutes = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_digital_twenty_four", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasDigitalTwentyFour = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_digital_colours", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasDigitalColours = true;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return ModuleSettings
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set hasTwentyFour.
     *
     * @param bool $hasTwentyFour
     *
     * @return ModuleSettings
     */
    public function setHasTwentyFour($hasTwentyFour)
    {
        $this->hasTwentyFour = $hasTwentyFour;

        return $this;
    }

    /**
     * Get hasTwentyFour.
     *
     * @return bool
     */
    public function getHasTwentyFour()
    {
        return $this->hasTwentyFour;
    }

    /**
     * Set hasQuadrants.
     *
     * @param bool $hasQuadrants
     *
     * @return ModuleSettings
     */
    public function setHasQuadrants($hasQuadrants)
    {
        $this->hasQuadrants = $hasQuadrants;

        return $this;
    }

    /**
     * Get hasQuadrants.
     *
     * @return bool
     */
    public function getHasQuadrants()
    {
        return $this->hasQuadrants;
    }

    /**
     * Set hasSound.
     *
     * @param bool $hasSound
     *
     * @return ModuleSettings
     */
    public function setHasSound($hasSound)
    {
        $this->hasSound = $hasSound;

        return $this;
    }

    /**
     * Get hasSound.
     *
     * @return bool
     */
    public function getHasSound()
    {
        return $this->hasSound;
    }

    /**
     * Set hasMascot.
     *
     * @param bool $hasMascot
     *
     * @return ModuleSettings
     */
    public function setHasMascot($hasMascot)
    {
        $this->hasMascot = $hasMascot;

        return $this;
    }

    /**
     * Get hasMascot.
     *
     * @return bool
     */
    public function getHasMascot()
    {
        return $this->hasMascot;
    }

    /**
     * Set hasColours.
     *
     * @param bool $hasColours
     *
     * @return ModuleSettings
     */
    public function setHasColours($hasColours)
    {
        $this->hasColours = $hasColours;

        return $this;
    }

    /**
     * Get hasColours.
     *
     * @return bool
     */
    public function getHasColours()
    {
        return $this->hasColours;
    }

    /**
     * Set hasColoursSwitched.
     *
     * @param bool $hasColoursSwitched
     *
     * @return ModuleSettings
     */
    public function setHasSwitchedColours($hasColoursSwitched)
    {
        $this->hasColoursSwitched = $hasColoursSwitched;

        return $this;
    }

    /**
     * Get hasColoursSwitched.
     *
     * @return bool
     */
    public function getHasColoursSwitched()
    {
        return $this->hasColoursSwitched;
    }

    /**
     * Set hasAnalogueHours.
     *
     * @param bool $hasAnalogueHours
     *
     * @return ModuleSettings
     */
    public function setHasAnalogueHours($hasAnalogueHours)
    {
        $this->hasAnalogueHours = $hasAnalogueHours;

        return $this;
    }

    /**
     * Get hasAnalogueHours.
     *
     * @return bool
     */
    public function getHasAnalogueHours()
    {
        return $this->hasAnalogueHours;
    }

    /**
     * Set hasAnalogueMinutes.
     *
     * @param bool $hasAnalogueMinutes
     *
     * @return ModuleSettings
     */
    public function setHasAnalogueMinutes($hasAnalogueMinutes)
    {
        $this->hasAnalogueMinutes = $hasAnalogueMinutes;

        return $this;
    }

    /**
     * Get hasAnalogueMinutes.
     *
     * @return bool
     */
    public function getHasAnalogueMinutes()
    {
        return $this->hasAnalogueMinutes;
    }

    /**
     * Set hasAnalogueDialNumbers.
     *
     * @param bool $hasAnalogueDialNumbers
     *
     * @return ModuleSettings
     */
    public function setHasAnalogueDialNumbers($hasAnalogueDialNumbers)
    {
        $this->hasAnalogueDialNumbers = $hasAnalogueDialNumbers;

        return $this;
    }

    /**
     * Get hasAnalogueDialNumbers.
     *
     * @return bool
     */
    public function getHasAnalogueDialNumbers()
    {
        return $this->hasAnalogueDialNumbers;
    }

    /**
     * Set hasAnalogueColours.
     *
     * @param bool $hasAnalogueColours
     *
     * @return ModuleSettings
     */
    public function setHasAnalogueColours($hasAnalogueColours)
    {
        $this->hasAnalogueColours = $hasAnalogueColours;

        return $this;
    }

    /**
     * Get hasAnalogueColours.
     *
     * @return bool
     */
    public function getHasAnalogueColours()
    {
        return $this->hasAnalogueColours;
    }

    /**
     * Set hasDigitalHours.
     *
     * @param bool $hasDigitalHours
     *
     * @return ModuleSettings
     */
    public function setHasDigitalHours($hasDigitalHours)
    {
        $this->hasDigitalHours = $hasDigitalHours;

        return $this;
    }

    /**
     * Get hasDigitalHours.
     *
     * @return bool
     */
    public function getHasDigitalHours()
    {
        return $this->hasDigitalHours;
    }

    /**
     * Set hasDigitalMinutes.
     *
     * @param bool $hasDigitalMinutes
     *
     * @return ModuleSettings
     */
    public function setHasDigitalMinutes($hasDigitalMinutes)
    {
        $this->hasDigitalMinutes = $hasDigitalMinutes;

        return $this;
    }

    /**
     * Get hasDigitalMinutes.
     *
     * @return bool
     */
    public function getHasDigitalMinutes()
    {
        return $this->hasDigitalMinutes;
    }

    /**
     * Set hasDigitalTwentyFour.
     *
     * @param bool $hasDigitalTwentyFour
     *
     * @return ModuleSettings
     */
    public function setHasDigitalTwentyFour($hasDigitalTwentyFour)
    {
        $this->hasDigitalTwentyFour = $hasDigitalTwentyFour;

        return $this;
    }

    /**
     * Get hasDigitalTwentyFour.
     *
     * @return bool
     */
    public function getHasDigitalTwentyFour()
    {
        return $this->hasDigitalTwentyFour;
    }

    /**
     * Set hasDigitalColours.
     *
     * @param bool $hasDigitalColours
     *
     * @return ModuleSettings
     */
    public function setHasDigitalColours($hasDigitalColours)
    {
        $this->hasDigitalColours = $hasDigitalColours;

        return $this;
    }

    /**
     * Get hasDigitalColours.
     *
     * @return bool
     */
    public function getHasDigitalColours()
    {
        return $this->hasDigitalColours;
    }
}
