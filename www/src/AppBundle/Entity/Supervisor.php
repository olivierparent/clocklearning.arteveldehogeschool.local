<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Supervisor.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 *
 * @ORM\Table(name="users_supervisors")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SupervisorRepository")
 */
class Supervisor extends UserAbstract
{
    const PROFESSION_THERAPIST = 'THERAPIST';
    const PROFESSION_TEACHER = 'TEACHER';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var ExerciseSettings
     *
     * @ORM\ManyToOne(targetEntity="ExerciseSettings")
     * @ORM\JoinColumn(name="exercise_settings_id")
     */
    private $exerciseSettings;

    /**
     * @var ModuleSettings
     *
     * @ORM\ManyToOne(targetEntity="ModuleSettings")
     * @ORM\JoinColumn(name="module_settings_id")
     */
    private $moduleSettings;

    /**
     * @var string
     *
     * @ORM\Column(name="profession", type="string", length=255)
     */
    private $profession;

    /**
     * @var array
     */
    protected $roles = [self::ROLE_SUPERVISOR];

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set exercise settings.
     *
     * @param ExerciseSettings $exerciseSettings
     *
     * @return Supervisor
     */
    public function setExerciseSettings(ExerciseSettings $exerciseSettings)
    {
        $this->exerciseSettings = $exerciseSettings;

        return $this;
    }

    /**
     * Get exercise settings.
     *
     * @return ExerciseSettings
     */
    public function getExerciseSettings()
    {
        return $this->exerciseSettings;
    }

    /**
     * Set module settings.
     *
     * @param ModuleSettings $moduleSettings
     *
     * @return Supervisor
     */
    public function setModuleSettings(ModuleSettings $moduleSettings)
    {
        $this->moduleSettings = $moduleSettings;

        return $this;
    }

    /**
     * Get module settings.
     *
     * @return ModuleSettings
     */
    public function getModuleSettings()
    {
        return $this->moduleSettings;
    }

    /**
     * Set profession.
     *
     * @param string $profession
     *
     * @return Supervisor
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;

        return $this;
    }

    /**
     * Get profession.
     *
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }
}
