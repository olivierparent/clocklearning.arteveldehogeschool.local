<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ExerciseSettings.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 *
 * @ORM\Table(name="exercise_settings")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExerciseSettingsRepository")
 */
class ExerciseSettings
{
    // Increments
    const MINUTES_INCREMENT_OFF = 0;
    const MINUTES_INCREMENT_01 = 1;
    const MINUTES_INCREMENT_05 = 5;
    const MINUTES_INCREMENT_10 = 10;

    // Modes
    const MODE_EXERCISE = 'EXERCISE';
    const MODE_TEST = 'TEST';

    // Assistance
    const ASSISTANCE_VERBAL = 'VERBAL';
    const ASSISTANCE_VISUAL = 'VISUAL';
    const ASSISTANCE_MODELLING = 'MODELLING';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var int
     *
     * @ORM\Column(name="amount", type="smallint", nullable=false, options={"default" = 10})
     */
    private $amount = 10;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_full_hour", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasFullHour = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_quarter_past", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasQuarterPast = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_half_hour", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasHalfHour = true;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_quarter_to", type="boolean", nullable=false, options={"default" = true})
     */
    private $hasQuarterTo = true;

    /**
     * @var int
     *
     * @ORM\Column(name="minutes_increment", type="smallint", nullable=false, options={"default" = ExerciseSettings::MINUTES_INCREMENT_OFF})
     */
    private $minutesIncrement = self::MINUTES_INCREMENT_OFF;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_seconds", type="boolean", nullable=false)
     */
    private $hasSeconds = false;

    /**
     * @var string
     *
     * @ORM\Column(name="mode", type="string", length=255, nullable=false, options={"default" = ExerciseSettings::MODE_EXERCISE})
     */
    private $mode = self::MODE_EXERCISE;

    /**
     * @var string
     *
     * @ORM\Column(name="assistance", type="string", length=255, nullable=false)
     */
    private $assistance = self::ASSISTANCE_MODELLING;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_subtitles", type="boolean", nullable=false, options={"default" = false})
     */
    private $hasSubtitles = false;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return ExerciseSettings
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set amount.
     *
     * @param int $amount
     *
     * @return ExerciseSettings
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set hasFullHour.
     *
     * @param bool $hasFullHour
     *
     * @return ExerciseSettings
     */
    public function setHasFullHour($hasFullHour)
    {
        $this->hasFullHour = $hasFullHour;

        return $this;
    }

    /**
     * Get hasFullHour.
     *
     * @return bool
     */
    public function getHasFullHour()
    {
        return $this->hasFullHour;
    }

    /**
     * Set hasQuarterPast.
     *
     * @param bool $hasQuarterPast
     *
     * @return ExerciseSettings
     */
    public function setHasQuarterPast($hasQuarterPast)
    {
        $this->hasQuarterPast = $hasQuarterPast;

        return $this;
    }

    /**
     * Get hasQuarterPast.
     *
     * @return bool
     */
    public function getHasQuarterPast()
    {
        return $this->hasQuarterPast;
    }

    /**
     * Set hasHalfHour.
     *
     * @param bool $hasHalfHour
     *
     * @return ExerciseSettings
     */
    public function setHasHalfHour($hasHalfHour)
    {
        $this->hasHalfHour = $hasHalfHour;

        return $this;
    }

    /**
     * Get hasHalfHour.
     *
     * @return bool
     */
    public function getHasHalfHour()
    {
        return $this->hasHalfHour;
    }

    /**
     * Set hasQuarterTo.
     *
     * @param bool $hasQuarterTo
     *
     * @return ExerciseSettings
     */
    public function setHasQuarterTo($hasQuarterTo)
    {
        $this->hasQuarterTo = $hasQuarterTo;

        return $this;
    }

    /**
     * Get hasQuarterTo.
     *
     * @return bool
     */
    public function getHasQuarterTo()
    {
        return $this->hasQuarterTo;
    }

    /**
     * Set minutesIncrement.
     *
     * @param string $minutesIncrement
     *
     * @return ExerciseSettings
     *
     * @throws \ErrorException
     */
    public function setMinutesIncrement($minutesIncrement)
    {
        switch ($minutesIncrement) {
            case self::MINUTES_INCREMENT_OFF:
            case self::MINUTES_INCREMENT_01:
            case self::MINUTES_INCREMENT_05:
            case self::MINUTES_INCREMENT_10:
                $this->minutesIncrement = $minutesIncrement;
                break;
            default:
                throw new \ErrorException('Invalid value for $minutesIncrement');
        }

        return $this;
    }

    /**
     * Get minutesIncrement.
     *
     * @return string
     */
    public function getMinutesIncrement()
    {
        return $this->minutesIncrement;
    }

    /**
     * Set hasSeconds.
     *
     * @param bool $hasSeconds
     *
     * @return ExerciseSettings
     */
    public function setHasSeconds($hasSeconds)
    {
        $this->hasSeconds = $hasSeconds;

        return $this;
    }

    /**
     * Get hasSeconds.
     *
     * @return bool
     */
    public function getHasSeconds()
    {
        return $this->hasSeconds;
    }

    /**
     * Set mode.
     *
     * @param string $mode
     *
     * @return ExerciseSettings
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    /**
     * Get mode.
     *
     * @return string
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * Set assistance.
     *
     * @param string $assistance
     *
     * @return ExerciseSettings
     */
    public function setAssistance($assistance)
    {
        $this->assistance = $assistance;

        return $this;
    }

    /**
     * Get assistance.
     *
     * @return string
     */
    public function getAssistance()
    {
        return $this->assistance;
    }

    /**
     * Set hasSubtitles.
     *
     * @param bool $hasSubtitles
     *
     * @return ExerciseSettings
     */
    public function setHasSubtitles($hasSubtitles)
    {
        $this->hasSubtitles = $hasSubtitles;

        return $this;
    }

    /**
     * Get hasSubtitles.
     *
     * @return bool
     */
    public function getHasSubtitles()
    {
        return $this->hasSubtitles;
    }
}
