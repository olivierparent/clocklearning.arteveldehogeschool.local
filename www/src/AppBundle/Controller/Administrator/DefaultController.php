<?php

namespace AppBundle\Controller\Administrator;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
// Annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/administrator", name="rikatika_administrator_default_index")
     */
    public function indexAction()
    {
        return $this->render('administrator/index.html.twig');
    }
}
