<?php

namespace AppBundle\Controller\Administrator;

use AppBundle\Entity\Administrator;
use AppBundle\Form\Administrator\LoginType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
// Annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class SecurityController.
 *
 * @Route("/administrator")
 */
class SecurityController extends Controller
{
    /**
     * @Route("/login", name="rikatika_administrator_security_login")
     * @Template("administrator/security/login.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function loginAction(Request $request)
    {
        $entity = new Administrator();
        $formType = new LoginType();

        $form = $this->createForm($formType, $entity, [
            'action' => $this->generateUrl('rikatika_administrator_security_login_check'),
        ]);

//        dump($request);

        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
        } else {
            $session = $request->getSession();
            $error = $session->get(Security::AUTHENTICATION_ERROR);
            $session->remove(Security::AUTHENTICATION_ERROR);
        }

        // Return array with variables for Twig.
        return [
            'form' => $form->createView(),
            'error' => $error,
        ];
    }
}
