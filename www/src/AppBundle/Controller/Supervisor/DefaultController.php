<?php

namespace AppBundle\Controller\Supervisor;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/supervisor", name="rikatika_supervisor_default_index")
     */
    public function indexAction()
    {
        return $this->render('supervisor/index.html.twig');
    }
}
