<?php

namespace AppBundle\Controller\Supervisor;

use AppBundle\Entity\Supervisor;
use AppBundle\Form\Supervisor\LoginType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
// Annotations
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class SecurityController.
 *
 * @Route("/supervisor")
 */
class SecurityController extends Controller
{
    /**
     * @Route("/login", name="rikatika_supervisor_security_login")
     * @Template("supervisor/security/login.html.twig")
     *
     * @param Request $request
     *
     * @return array
     */
    public function loginAction(Request $request)
    {
        $entity = new Supervisor();
        $formType = new LoginType();

        $form = $this->createForm($formType, $entity, [
            'action' => $this->generateUrl('rikatika_supervisor_security_login_check'),
        ]);

//        dump($request);

        if ($request->attributes->has(Security::AUTHENTICATION_ERROR)) {
            $error = $request->attributes->get(Security::AUTHENTICATION_ERROR);
        } else {
            $session = $request->getSession();
            $error = $session->get(Security::AUTHENTICATION_ERROR);
            $session->remove(Security::AUTHENTICATION_ERROR);
        }

        // Return array with variables for Twig.
        return [
            'form' => $form->createView(),
            'error' => $error,
        ];
    }
}
