<?php

namespace AppBundle\Controller\Member;

// Annotations
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/member", name="rikatika_member_default_index")
     */
    public function indexAction()
    {
        return $this->render('member/index.html.twig');
    }
}
