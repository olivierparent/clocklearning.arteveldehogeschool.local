<?php

namespace AppBundle\Form\Administrator;

use AppBundle\Form\AdministratorType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginType extends AdministratorType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, [
                'label' => 'label.username',
            ])
            ->add('password', 'password', [
                'label' => 'label.password'
            ])
            ->add('btn_login', 'submit', [
                'label' => 'label.sign_on',
            ])
        ;
    }

    /**
     * Form name.
     *
     * @return string
     */
    public function getName()
    {
        return parent::getName() . '_login';
    }
}
