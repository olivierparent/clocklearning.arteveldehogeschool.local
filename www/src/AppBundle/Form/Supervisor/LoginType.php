<?php

namespace AppBundle\Form\Supervisor;

use AppBundle\Form\SupervisorType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginType extends SupervisorType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, [
                'label' => 'label.username',
            ])
            ->add('password', 'password', [
                'label' => 'label.password'
            ])
            ->add('btn_login', 'submit', [
                'label' => 'label.sign_on',
            ])
        ;
    }

    /**
     * Form name.
     *
     * @return string
     */
    public function getName()
    {
        return parent::getName() . '_login';
    }
}
