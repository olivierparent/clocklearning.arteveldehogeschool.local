<?php

namespace AppBundle\RikaTika\TimeGenerator;

/**
 * TimeGeneratorNL.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class TimeGeneratorNL extends TimeGeneratorAbstract
{
    const ANTE_MERIDIEM = '’s&nbsp;morgens';
    const FORMAT_ANALOGUE_HALF = 'half %1$d';
    const FORMAT_ANALOGUE_PAST = '%2$s over %1$d';
    const FORMAT_ANALOGUE_PAST_HALF = '%2$s over half %1$d';
    const FORMAT_ANALOGUE_TO_HALF = '%2$s voor half %1$d';
    const FORMAT_ANALOGUE_TO = '%2$s voor %1$s';
    const FORMAT_DIGITAL = '%1$d uur %2$d';
    const FORMAT_FULL_HOUR = '%1$d uur';
    const MINUTE = 'minuut';
    const MINUTES = 'minuten';
    const QUARTER = 'kwart';
    const POST_MERIDIEM = '’s&nbsp;middags';

    /**
     * Convert time in hours and minutes to a formatted text string.
     *
     * @param string $type
     *
     * @return string
     *
     * @throws \ErrorException
     */
    public function toText($type = self::TEXT_ANALOGUE)
    {
        $h = $this->getHour();
        $m = $this->getMinute();

        if (0 <= $h && $h < 24) {
            if (0 === $h) {
                $h = 12;
            } elseif (12 < $h) {
                $h -= 12;
            }
        } else {
            throw new \ErrorException("Illegal value <strong>{$h}</strong> for hours in class <strong>".get_called_class().'</strong>');
        }

        switch ($type) {
            case self::TEXT_DIGITAL:
            case self::TEXT_DIGITAL_REVERSE_M_DIGITS:
            case self::TEXT_DIGITAL_SWAP_H_AND_M:
            case self::TEXT_DIGITAL_SWAP_H_AND_M_THEN_REVERSE_H_DIGITS:
                if (0 === $m) {
                    if (self::$_checkImaginary) {
                        $format = self::FORMAT_DIGITAL;
                    } else {
                        $format = self::FORMAT_FULL_HOUR;
                    }
                } else {
                    $format = self::FORMAT_DIGITAL;
                }
                break;

            case self::TEXT_DIGITAL_H×100:
                $h *= 10;
//                break; Fall through!
            case self::TEXT_DIGITAL_H×10:
                $h *= 10;
                $format = self::FORMAT_FULL_HOUR;
                break;

            case self::TEXT_DIGITAL_M00_AS_0:
                $format = self::FORMAT_DIGITAL;
                break;

            case self::TEXT_ANALOGUE_PAST:
            case self::TEXT_ANALOGUE_SWAP_H_AND_M_THEN_PAST:
                $format = self::FORMAT_ANALOGUE_PAST;
                break;

            case self::TEXT_ANALOGUE_TO:
                $format = self::FORMAT_ANALOGUE_TO;
                break;

            case self::TEXT_ANALOGUE:
            case self::TEXT_ANALOGUE_SWAP_H_AND_M:
            case self::TEXT_ANALOGUE_QUARTERS_AS_NUMBER:
            default:
                if (0 <= $m && $m < 30) {
                    if (0 === $m) {
                        $format = self::FORMAT_FULL_HOUR;
                    } else {
                        $format = self::FORMAT_ANALOGUE_PAST;
                        if (15 === $m && !($type === self::TEXT_ANALOGUE_SWAP_H_AND_M ||
                                $type === self::TEXT_ANALOGUE_QUARTERS_AS_NUMBER)) {
                            $m = self::QUARTER;
                        } else {
                            if (self::getModuleSettings()->getHasQuadrants() && 15 < $m) {
                                $format = self::FORMAT_ANALOGUE_TO_HALF;
                                if (12 === $h++) {
                                    $h = 1;
                                }
                                $m = 30 - $m;
                            }
                        }
                    }
                } elseif ((30 <= $m && $m < 60)) {
                    if (12 === $h++) {
                        $h = 1;
                    }
                    if (30 === $m) {
                        $format = self::FORMAT_ANALOGUE_HALF;
                    } else {
                        $format = self::FORMAT_ANALOGUE_TO;
                        if (45 === $m && !($type === self::TEXT_ANALOGUE_SWAP_H_AND_M ||
                                $type === self::TEXT_ANALOGUE_QUARTERS_AS_NUMBER)) {
                            $m = self::QUARTER;
                        } else {
                            if (self::getModuleSettings()->getHasQuadrants() && $m < 45) {
                                $format = self::FORMAT_ANALOGUE_PAST_HALF;
                                $m -= 30;
                            } else {
                                $m = 60 - $m;
                            }
                        }
                    }
                } else {
                    throw new \ErrorException("Illegal value <strong>{$m}</strong> for minutes in class <strong>".get_called_class().'</strong>');
                }
                break;
        }

        // POSITION
        switch ($type) {
            case self::TEXT_ANALOGUE_SWAP_H_AND_M:
            case self::TEXT_ANALOGUE_SWAP_H_AND_M_THEN_PAST:
            case self::TEXT_DIGITAL_SWAP_H_AND_M:
            case self::TEXT_DIGITAL_SWAP_H_AND_M_THEN_REVERSE_H_DIGITS:
                $former = $m;
                $latter = $h;
                break;
            default:
                $former = $h;
                $latter = $m;
                break;
        }

        // REVERSE DIGITS
        switch ($type) {
            case self::TEXT_DIGITAL_REVERSE_M_DIGITS:
                if (10 <= $latter) {
                    $latter = (int) strrev("{$latter}");
                }
                break;
            case self::TEXT_DIGITAL_SWAP_H_AND_M_THEN_REVERSE_H_DIGITS:
                if (10 <= $former) {
                    $former = (int) strrev("{$former}");
                }
                break;
            default:
                break;
        }

        // ADD 'MINUTE'/'MINUTES' to ANALOGUE
        switch ($type) {
            case self::TEXT_ANALOGUE:
            case self::TEXT_ANALOGUE_QUARTERS_AS_NUMBER:
            case self::TEXT_ANALOGUE_SWAP_H_AND_M:
                if (is_int($latter)) {
                    $latter .= ' '.(1 === $latter ? self::MINUTE : self::MINUTES);
                }
                break;
            default:
                break;
        }

//        $format .= ' ' . ($this->Hours < 12 ? self::ANTE_MERIDIEM : self::POST_MERIDIEM);

        return sprintf($format, $former, $latter);
    }
}
