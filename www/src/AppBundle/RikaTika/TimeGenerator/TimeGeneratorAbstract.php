<?php

namespace AppBundle\RikaTika\TimeGenerator;

use AppBundle\Entity\ExerciseSettings;
use AppBundle\Entity\ModuleSettings;

/**
 * TimeGeneratorAbstract.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
abstract class TimeGeneratorAbstract
{
    const H_MINUTES = 60;
    const M_SECONDS = 60;
    const H_SECONDS = self::H_MINUTES * self::M_SECONDS; // PHP 5.6+
    const H_RANGE_12 = [1 ,12]; // PHP 5.6+
    const H_RANGE_24 = [0 ,23]; // PHP 5.6+
    const M_RANGE = [0, 59]; // PHP 5.6+
    const S_RANGE = [0, 59]; // PHP 5.6+

    // Analogue clock
    const TEXT_ANALOGUE = 'TEXT_ANALOGUE';
    const TEXT_ANALOGUE_PAST = 'TEXT_ANALOGUE_PAST';
    const TEXT_ANALOGUE_TO = 'TEXT_ANALOGUE_TO';
    const TEXT_ANALOGUE_QUARTERS_AS_NUMBER = 'TEXT_ANALOGUE_QUARTERS_AS_NUMBER';
    const TEXT_ANALOGUE_SWAP_H_AND_M = 'TEXT_ANALOGUE_SWAP_H_AND_M';
    const TEXT_ANALOGUE_SWAP_H_AND_M_THEN_PAST = 'TEXT_ANALOGUE_SWAP_H_AND_M_THEN_PAST';

    const CORRECT_ANALOGUE = 'correctAnalogue';

    const ERROR_ANALOGUE_H_00_12_M_00 = 'errorAnalogueH0012M00';
    const ERROR_ANALOGUE_H_03_15_M_15 = 'errorAnalogueH0315M15';
    const ERROR_ANALOGUE_H_05_17_M_30 = 'errorAnalogueH0517M30';
    const ERROR_ANALOGUE_H_09_21_M_45 = 'errorAnalogueH0921M45';
    const ERROR_ANALOGUE_HOURS_HALF_HOUR_EARLIER = 'errorAnalogueHoursHalfHourEarlier';
    const ERROR_ANALOGUE_HOURS_HALF_HOUR_LATER = 'errorAnalogueHoursHalfHourLater';
    const ERROR_ANALOGUE_HOURS_HOUR_EARLIER = 'errorAnalogueHoursHourEarlier';
    const ERROR_ANALOGUE_HOURS_HOUR_LATER = 'errorAnalogueHoursHourLater';
    const ERROR_ANALOGUE_HOURS_MIRROR_Y = 'errorAnalogueHoursMirrorY';
    const ERROR_ANALOGUE_HOURS_MIRROR_Z = 'errorAnalogueHoursMirrorZ';
    const ERROR_ANALOGUE_MINUTES_01M_EARLIER = 'errorAnalogueMinutes01mEarlier';
    const ERROR_ANALOGUE_MINUTES_01M_DIFFERENCE = 'errorAnalogueMinutes01mDifference';
    const ERROR_ANALOGUE_MINUTES_01M_LATER = 'errorAnalogueMinutes01mLater';
    const ERROR_ANALOGUE_MINUTES_05M_EARLIER = 'errorAnalogueMinutes05mEarlier';
    const ERROR_ANALOGUE_MINUTES_05M_DIFFERENCE = 'errorAnalogueMinutes05mDifference';
    const ERROR_ANALOGUE_MINUTES_05M_LATER = 'errorAnalogueMinutes05mLater';
    const ERROR_ANALOGUE_MINUTES_MIRROR_Y = 'errorAnalogueMinutesMirrorY';
    const ERROR_ANALOGUE_MINUTES_MIRROR_Z = 'errorAnalogueMinutesMirrorZ';
    const ERROR_ANALOGUE_MINUTES_READ_AS_ADDITION_TO_HOURS = 'errorAnalogueMinutesReadAsAdditionToHours';
    const ERROR_ANALOGUE_MINUTES_READ_AS_HOURS = 'errorAnalogueMinutesReadAsHours';
    const ERROR_ANALOGUE_MINUTES_READ_AS_HOURS_WITH_PAST_TO = 'errorAnalogueMinutesReadAsHoursWithPastTo';
    const ERROR_ANALOGUE_READ_AS_FULL_HOURS = 'errorAnalogueReadAsFullHours';
    const ERROR_ANALOGUE_SWITCH_HANDS = 'errorAnalogueSwitchHands';
    const ERROR_ANALOGUE_SWITCH_HANDS_NEAREST_HOUR = 'errorAnalogueSwitchHandsNearestHour';
    const ERROR_ANALOGUE_SWITCH_HANDS_READ_AS_FULL_HOURS = 'errorAnalogueSwitchHandsReadAsFullHours';
    const ERROR_ANALOGUE_SWITCH_HANDS_READ_AS_HOURS = 'errorAnalogueSwitchHandsReadAsHours';
    const ERROR_ANALOGUE_SWITCH_HANDS_READ_AS_HOURS_WITH_PAST_TO = 'errorAnalogueSwitchHandsReadAsHoursWithPastTo';
    const ERROR_ANALOGUE_SWITCH_HANDS_READ_AS_MINUTES_WITH_PAST_TO = 'errorAnalogueSwitchHandsReadAsMinutesWithPastTo';

    // Digital clock
    const TEXT_DIGITAL = 'TEXT_DIGITAL';
    const TEXT_DIGITAL_H×10 = 'TEXT_DIGITAL_H×10';
    const TEXT_DIGITAL_H×100 = 'TEXT_DIGITAL_H×100';
    const TEXT_DIGITAL_M00_AS_0 = 'TEXT_DIGITAL_M00_AS_0';
    const TEXT_DIGITAL_REVERSE_M_DIGITS = 'TEXT_DIGITAL_REVERSE_M_DIGITS';
    const TEXT_DIGITAL_SWAP_H_AND_M = 'TEXT_DIGITAL_SWAP_H_AND_M';
    const TEXT_DIGITAL_SWAP_H_AND_M_THEN_REVERSE_H_DIGITS = 'TEXT_DIGITAL_SWAP_H_AND_M_THEN_REVERSE_H_DIGITS';

    const CORRECT_DIGITAL = 'correctDigital';
    const CORRECT_DIGITAL_READ_ANALOGUE = 'correctDigitalReadAnalogue';
    const CORRECT_DIGITAL_READ_ANALOGUE_PAST = 'correctDigitalReadAnaloguePast';

    const ERROR_DIGITAL_EARLIER_BY_30M = 'errorDigitalEarlierBy30m';
    const ERROR_DIGITAL_EARLIER_BY_60M = 'errorDigitalEarlierBy60m';
    const ERROR_DIGITAL_READ_H×10 = 'errorDigitalReadH×10';
    const ERROR_DIGITAL_READ_H×100 = 'errorDigitalReadH×100';
    const ERROR_DIGITAL_READ_HOUR = 'errorDigitalReadHour';
    const ERROR_DIGITAL_READ_M00_AS_0 = 'errorDigitalReadM00As0';
    const ERROR_DIGITAL_READ_REVERSE_M_DIGITS = 'errorDigitalReadReverseMDigits';
    const ERROR_DIGITAL_READ_QUARTERS_AS_NUMBER = 'errorDigitalReadQuartersAsNumber';
    const ERROR_DIGITAL_READ_SWAP_H_AND_M = 'errorDigitalReadSwapHAndM';
    const ERROR_DIGITAL_READ_SWAP_H_AND_M_THEN_PAST = 'errorDigitalReadSwapHAndMThenPast';
    const ERROR_DIGITAL_READ_SWAP_H_AND_M_THEN_REVERSE_H_DIGITS = 'errorDigitalReadSwapHAndMThenReverseHDigits';
    const ERROR_DIGITAL_READ_TO = 'errorDigitalReadTo';
    const ERROR_DIGITAL_SWAP_PAST_AND_TO = 'errorDigitalSwapPastAndTo';
    const ERROR_DIGITAL_SWAP_PAST_AND_TO_THEN_READ_QUARTERS_AS_NUMBER = 'errorDigitalSwapPastAndToThenReadQuartersAsNumber';

    /**
     * @var null|ExerciseSettings
     */
    protected static $exerciseSettings = null;

    /**
     * @var null|ModuleSettings
     */
    protected static $moduleSettings = null;

    /**
     * Hour.
     *
     * @var int
     */
    protected $hour;

    /**
     * Minute.
     *
     * @var int
     */
    protected $minute;

    /**
     * Second.
     *
     * @var int
     */
    protected $second;

    /**
     * Answer Mode.
     *
     * @var string
     */
    protected $answerMode;

    /**
     * Get exercise settings.
     *
     * @return ExerciseSettings
     */
    public static function getExerciseSettings()
    {
        if (!(self::$exerciseSettings instanceof ExerciseSettings)) {
            self::setExerciseSettings();
        }

        return self::$exerciseSettings;
    }

    /**
     * Set exercise settings.
     *
     * @param ExerciseSettings $exerciseSettings
     */
    public static function setExerciseSettings(ExerciseSettings $exerciseSettings = null)
    {
        self::$exerciseSettings = ($exerciseSettings instanceof ExerciseSettings) ? $exerciseSettings : new ExerciseSettings();
    }

    /**
     * Get module settings.
     *
     * @return ModuleSettings
     */
    public static function getModuleSettings()
    {
        if (!(self::$moduleSettings instanceof ModuleSettings)) {
            self::setModuleSettings();
        }

        return self::$moduleSettings;
    }

    /**
     * Set module settings.
     *
     * @param ModuleSettings $moduleSettings
     */
    public static function setModuleSettings(ModuleSettings $moduleSettings = null)
    {
        self::$moduleSettings = ($moduleSettings instanceof ModuleSettings) ?  $moduleSettings : new ModuleSettings();
    }

    /**
     * Get hour range according to 24-hour or 12-hour clock.
     *
     * @return array
     */
    public static function getHourRange()
    {
        return self::getModuleSettings()->getHasTwentyFour() ? self::H_RANGE_24 : self::H_RANGE_12;
    }

    /**
     * Get maximum hour integer according to 24-hour or 12-hour clock.
     *
     * @return int
     */
    public static function getHourMax()
    {
        $range = self::getHourRange();

        return $range[1];
    }

    /**
     * Get minimum hour integer according to 24-hour or 12-hour clock.
     *
     * @return int
     */
    public static function getHourMin()
    {
        $range = self::getHourRange();

        return $range[0];
    }

    /**
     * Get minute range.
     *
     * @return array
     */
    public static function getMinuteRange()
    {
        return self::M_RANGE;
    }

    /**
     * Get maximum minute integer.
     *
     * @return int
     */
    public static function getMinuteMax()
    {
        $range = self::getMinuteRange();

        return $range[1];
    }

    /**
     * Get minimum minute integer.
     *
     * @return int
     */
    public static function getMinuteMin()
    {
        $range = self::getMinuteRange();

        return $range[0];
    }

    /**
     * Get minute range.
     *
     * @return array
     */
    public static function getSecondRange()
    {
        return self::S_RANGE;
    }

    /**
     * Get maximum second integer.
     *
     * @return int
     */
    public static function getSecondMax()
    {
        $range = self::getSecondRange();

        return $range[1];
    }

    /**
     * Get minimum second integer.
     *
     * @return int
     */
    public static function getSecondMin()
    {
        $range = self::getSecondRange();

        return $range[0];
    }

    /**
     * Constructor method.
     *
     * @param int $hour
     * @param int $minute
     */
    public function __construct($hour = null, $minute = null)
    {
        // Make sure, the static class has Exercise and Module settings prior to setTime()
        self::getExerciseSettings();
        self::getModuleSettings();

        $this->setTime($hour, $minute);
    }

    /**
     * Magic Method called when object is (implicitly) converted to string.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toTime();
    }

    /**
     * Set time.
     *
     * @param int|null $hour
     * @param int|null $minute
     * @param int|null $second
     *
     * @return TimeGeneratorAbstract
     */
    public function setTime($hour = null, $minute = null, $second = null)
    {
        return $this
            ->setHour($hour)
            ->setMinute($minute)
            ->setSecond($second);
    }

    /**
     * Set hour.
     *
     * @param int|null $hour
     *
     * @return $this
     *
     * @throws \ErrorException
     */
    public function setHour($hour = null)
    {
        if (null === $hour) {
            $h = self::getModuleSettings()->getHasTwentyFour() ? self::getHourMin() : self::getHourMax();
        } else {
            $options = [
                'options' => [
                    'default' => false,
                    'min_range' => self::getHourMin(),
                    'max_range' => self::getHourMax(),
                ],
            ];

            $h = filter_var($hour, FILTER_VALIDATE_INT, $options);

            if (false === $h) {
                throw new \ErrorException('Illegal hour: '.$hour);
            }
        }

        $this->hour = $h;

        return $this;
    }

    /**
     * Get hour.
     *
     * @return int
     */
    public function getHour()
    {
        if (!self::getModuleSettings()->getHasTwentyFour()) {
            $this->hour = $this->to12HourClock($this->hour);
        }

        return $this->hour;
    }

    /**
     * Set minute.
     *
     * @param int|null $minute
     *
     * @return TimeGeneratorAbstract
     */
    public function setMinute($minute = null)
    {
        list($mMin, $mMax) = self::getMinuteRange();

        $options = [
            'options' => [
                'default' => $mMin,
                'min_range' => $mMin,
                'max_range' => $mMax,
            ],
        ];
        $this->minute = filter_var($minute, FILTER_VALIDATE_INT, $options);

        return $this;
    }

    /**
     * Get minute.
     *
     * @return int
     */
    public function getMinute()
    {
        return $this->minute;
    }

    /**
     * Set second.
     *
     * @param int|null $second
     *
     * @return TimeGeneratorAbstract
     */
    public function setSecond($second = null)
    {
        list($sMin, $sMax) = self::getSecondRange();

        if (self::getExerciseSettings()->getHasSeconds()) {
            $options = [
                'options' => [
                    'default' => $sMin,
                    'min_range' => $sMin,
                    'max_range' => $sMax,
                ],
            ];
            $this->second = filter_var($second, FILTER_VALIDATE_INT, $options);
        } else {
            $this->second = $sMin;
        }

        return $this;
    }

    /**
     * Get second.
     *
     * @return int
     */
    public function getSecond()
    {
        return self::getExerciseSettings()->getHasSeconds() ? $this->second : 0;
    }

    /**
     * Set time in seconds.
     *
     * @param int $timeInSeconds
     *
     * @return TimeGeneratorAbstract
     *
     * @throws \ErrorException
     */
    public function setTimeInSeconds($timeInSeconds = 0)
    {
        $h = (int) ($timeInSeconds / self::H_SECONDS);
        $m = (int) (($timeInSeconds % self::H_SECONDS) / self::M_SECONDS);
        $s = $timeInSeconds % self::H_SECONDS % self::M_SECONDS;

        if (!self::getModuleSettings()->getHasTwentyFour() && 0 === $h) {
            $h = 12;
        }

        return $this
            ->setHour($h)
            ->setMinute($m)
            ->setSecond($s);
    }

    /**
     * Get time in seconds.
     *
     * @return int
     */
    public function getTimeInSeconds()
    {
        $h = $this->getHour();
        $m = $this->getMinute();
        $s = $this->getSecond();

        if (!self::getModuleSettings()->getHasTwentyFour() && 12 === $h) {
            $h = 0;
        }

        return $h * self::H_SECONDS + $m * self::M_SECONDS + $s;
    }

    /**
     * Set answer mode.
     *
     * @param string $answerMode
     *
     * @return TimeGeneratorAbstract
     */
    public function setAnswerMode($answerMode)
    {
        $this->answerMode = $answerMode;

        return $this;
    }

    /**
     * Get answer mode.
     *
     * @return string
     */
    public function getAnswerMode()
    {
        return $this->answerMode;
    }

    use CommonTrait;

    use AnalogueTrait;

    use DigitalTrait;
}
