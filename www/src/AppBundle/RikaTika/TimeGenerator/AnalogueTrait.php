<?php

namespace AppBundle\RikaTika\TimeGenerator;

/**
 * AnalogueTrait.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
trait AnalogueTrait
{
    /**
     * Generate answer ERROR_ANALOGUE_H_00_12_M_00.
     *
     * Time on analogue clock is read as twelve o'clock.
     *
     * @return bool
     */
    public function errorAnalogueH0012M00()
    {
        if (self::getModuleSettings()->getHasTwentyFour()) {
            $this->setHour($this->getHour() < 12 ? 0 : 12);
        } else {
            $this->setHour(12);
        }
        $this
            ->setMinute(0)
            ->setAnswerMode(__FUNCTION__);

        return true;
    }

    /**
     * Generate answer ERROR_ANALOGUE_H_03_15_M_15.
     *
     * Time on analogue clock is read as quarter past three.
     *
     * @return bool
     */
    public function errorAnalogueH0315M15()
    {
        if (self::getModuleSettings()->getHasTwentyFour()) {
            $this->setHour($this->getHour() < 12 ? 3 : 15);
        } else {
            $this->setHour(3);
        }
        $this
            ->setMinute(15)
            ->setAnswerMode(__FUNCTION__);

        return true;
    }

    /**
     * Generate answer ERROR_ANALOGUE_H_05_17_M_30.
     *
     * Time on analogue clock is read as half past five.
     *
     * @return bool
     */
    public function errorAnalogueH0517M30()
    {
        if (self::getModuleSettings()->getHasTwentyFour()) {
            $this->setHour($this->getHour() < 12 ? 5 : 17);
        } else {
            $this->setHour(5);
        }
        $this
            ->setMinute(30)
            ->setAnswerMode(__FUNCTION__);

        return true;
    }

    /**
     * Generate answer ERROR_ANALOGUE_H_09_21_M_45.
     *
     * Time on analogue clock is read as a quarter to ten.
     *
     * @return bool
     */
    public function errorAnalogueH0921M45()
    {
        if (self::getModuleSettings()->getHasTwentyFour()) {
            $this->setHour($this->getHour() < 12 ? 9 : 21);
        } else {
            $this->setHour(9);
        }
        $this
            ->setMinute(45)
            ->setAnswerMode(__FUNCTION__);

        return true;
    }

    /**
     * Generate answer ERROR_ANALOGUE_HOURS_HALF_HOUR_EARLIER.
     *
     * Time on analogue clock is read as a half hour earlier.
     * Time is kept on same day half.
     *
     * @todo Keep on same day half?
     *
     * @return bool
     */
    public function errorAnalogueHoursHalfHourEarlier()
    {
        $this->deltaMinutes(-30);

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_HOURS_HALF_HOUR_LATER.
     *
     * Time on analogue clock is read as a half hour later.
     * Time is kept on same day half.
     *
     * @todo Keep on same day half?
     *
     * @return bool
     */
    public function errorAnalogueHoursHalfHourLater()
    {
        //        if (in_array($this->getHour(), [11, 23]) && $this->getMinute() <= 30) {
//            #$this->deltaMinutes(-660);
//            $this->deltaHours(-11);
//        } else {
            $this->deltaMinutes(+30);
//        }

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_HOURS_HOUR_EARLIER.
     *
     * Time on analogue clock is read as an hour earlier.
     * Time is kept on same day half.
     *
     * @todo Keep on same day half?
     *
     * @return bool
     */
    public function errorAnalogueHoursHourEarlier()
    {
        $hoursDelta = -1;

        if (self::getModuleSettings()->getHasTwentyFour() && (0 === $this->getHour() || 12 === $this->getHour())) {
            $hoursDelta += 12;
        }

        $this->deltaHours($hoursDelta);

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_HOURS_HOUR_LATER.
     *
     * Time on analogue clock is read as an hour later.
     * Time is kept on same day half.
     *
     * @todo Keep on same mediem?
     *
     * @return bool
     */
    public function errorAnalogueHoursHourLater()
    {
        $hoursDelta = 1;

        if (self::getModuleSettings()->getHasTwentyFour() && (11 === $this->getHour() || 23 === $this->getHour())) {
            $hoursDelta -= 12;
        }

        $this->deltaHours($hoursDelta);

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_HOURS_MIRROR_Y.
     *
     * The hours hand is read as if mirrored along the Y-axis.
     *
     * @return bool
     */
    public function errorAnalogueHoursMirrorY()
    {
        $this->setHour(($this->getHour() < 13 ? 12 : 24) - $this->getHour());

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_HOURS_MIRROR_Z.
     *
     * The hours hand is read as if mirrored along the Z-axis (6 hours difference).
     * Time is kept on same day half.
     *
     * @return bool
     */
    public function errorAnalogueHoursMirrorZ()
    {
        if ((0 <= $this->getHour() && $this->getHour() <=  6) ||
            (12 <= $this->getHour() && $this->getHour() <= 18)) {
            $this->deltaHours(+6);
        } else {
            $this->deltaHours(-6);
        }

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_MINUTES_01M_EARLIER.
     *
     * Time on analogue clock is read as 1 minute earlier.
     *
     * @return bool
     */
    public function errorAnalogueMinutes01mEarlier()
    {
        $this->deltaMinutes(-1);

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_MINUTES_01M_DIFFERENCE.
     *
     * Time on analogue clock is read with 1 minute difference.
     *
     * @return bool
     */
    public function errorAnalogueMinutes01mDifference()
    {
        switch ($this->getMinute() % 5) {
            case 0:
                return false;
                break;
            case 1:
                if ($this->getMinute() < 30) {
                    return $this->errorAnalogueMinutes01mLater();
                } else {
                    return $this->errorAnalogueMinutes01mEarlier();
                }
                break;
            case 4:
                if ($this->getMinute() < 30) {
                    return $this->errorAnalogueMinutes01mEarlier();
                } else {
                    return $this->errorAnalogueMinutes01mLater();
                }
                break;
            default:
                if (mt_rand(0, 1)) {
                    return $this->errorAnalogueMinutes01mEarlier();
                } else {
                    return $this->errorAnalogueMinutes01mLater();
                }
                break;
        }
    }

    /**
     * Generate answer ERROR_ANALOGUE_MINUTES_01M_LATER.
     *
     *  Time on analogue clock is read as 1 minute later.
     *
     * @return bool
     */
    public function errorAnalogueMinutes01mLater()
    {
        $this->deltaMinutes(+1);

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_MINUTES_05M_EARLIER.
     *
     * Five minutes earlier.
     *
     * @return bool
     */
    public function errorAnalogueMinutes05mEarlier()
    {
        $this->deltaMinutes(-5);

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_MINUTES_05M_DIFFERENCE.
     *
     * Five minutes difference.
     *
     * @return bool
     */
    public function errorAnalogueMinutes05mDifference()
    {
        if ($this->getMinute() < 30) {
            return $this->errorAnalogueMinutes05mLater();
        } else {
            return $this->errorAnalogueMinutes05mEarlier();
        }
    }

    /**
     * Generate answer ERROR_ANALOGUE_MINUTES_5_LATER.
     *
     * Five minutes later.
     *
     * @return bool
     */
    public function errorAnalogueMinutes05mLater()
    {
        $this->deltaMinutes(+5);

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_MINUTES_MIRROR_Y.
     *
     * @return bool
     */
    public function errorAnalogueMinutesMirrorY()
    {
        if (in_array($this->getMinute(), [0, 30])) {
            return false;
        } else {
            if ($this->getMinute() < 30) {
                if (self::getTwentyFour()) {
                    if (in_array($this->getHour(), [0, 12])) {
                        $this->deltaHours(+11);
                    } else {
                        $this->deltaHours(-1);
                    }
                } else {
                    $this->deltaHours(-1);
                }
            } else {
                $this->deltaHours(+1);
            }
            $this->setMinute(60 - $this->getMinute());
        }

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_MINUTES_MIRROR_Z.
     *
     * @return bool
     */
    public function errorAnalogueMinutesMirrorZ()
    {
        $this->deltaMinutes(30);

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_MINUTES_READ_AS_ADDITION_TO_HOURS.
     *
     * @return bool
     */
    public function errorAnalogueMinutesReadAsAdditionToHours()
    {
        $minutes = (int) ($this->getMinute() / 5);
        $minutesModulo = $this->getMinute() % 5;

        if (0 === $minutes) {
            $minutes += 12;
        }

        if ($this->getMinute() < 30) {
            $this->setMinute($minutes + ($minutes < 12 && $minutesModulo == 4 ? 0 : $minutesModulo));
        } else {
            $this->setMinute($minutes + ($minutes < 12 && $minutesModulo == 4 ? 0 : $minutesModulo));
            $this->errorAnalogueMinutesMirrorY();
        }

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_MINUTES_READ_AS_HOURS.
     *
     * @return bool
     */
    public function errorAnalogueMinutesReadAsHours()
    {
        $this->setMinute($this->_to12Hours($this->_toHours($this->getMinute())));

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_MINUTES_READ_AS_HOURS_WITH_PAST_TO.
     *
     * @return bool
     */
    public function errorAnalogueMinutesReadAsHoursWithPastTo()
    {
        $minutes = $this->_toHours($this->getMinute());

        if (0 === $minutes) {
            $minutes = 12;
        } elseif (15 === $minutes) {
            $minutes = 3;
        }

        $this->setMinute(($this->getMinute() <= 30) ? $minutes : 60 - $minutes);

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_READ_AS_FULL_HOURS.
     *
     * Only use when hands overlap.
     *
     * Set minutes to 0.
     *
     * @return bool
     */
    public function errorAnalogueReadAsFullHours()
    {
        $hours = $this->_to12Hours($this->getHour());
        $minutes = $this->_to12Hours($this->_toHours($this->getMinute()));

        // Return false if hands don't overlap.
        if ($hours !== $minutes) {
            return false;
        }

        $this->setMinute(0);

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_SWAP_HANDS.
     *
     * 1. Swap hands.
     *
     * @return bool
     */
    public function errorAnalogueSwapHands()
    {
        $hours = $this->_toHours($this->getMinute());
        $minutes = $this->_toMinutes($this->getHour());

        $this->setHour($hours);
        $this->setMinute($minutes);

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_SWAP_HANDS_NEAREST_HOUR.
     *
     * 1. Swap hands must have happened before.
     * 2. Keep hour hand to nearest hour marking.
     *
     * @return bool
     */
    public function errorAnalogueSwapHandsNearestHour()
    {
        // Keep hour hand to nearest hour marking.
        if ($this->getMinute() !== 0 && 30 <= $this->getMinute()) {
            $this->deltaHours(-1);
        }

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_SWAP_HANDS_READ_AS_FULL_HOURS.
     *
     * Only use when hands overlap.
     *
     * 1. Swap hands.
     * 2. Set minutes to 0.
     *
     * @return bool
     */
    public function errorAnalogueSwapHandsReadAsFullHours()
    {
        $hours = $this->_to12Hours($this->getHour());
        $minutes = $this->_to12Hours($this->_toHours($this->getMinute()));

        // Return false if hands don't overlap.
        if ($hours !== $minutes) {
            return false;
        }

        $this->setHour($this->_to24Hours($minutes));
        $this->setMinute(0);

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_SWAP_HANDS_READ_AS_HOURS.
     *
     * Only use when minutes are less than 23.
     *
     * @return bool
     */
    public function errorAnalogueSwapHandsReadAsHours()
    {
        if (23 < $this->getMinute()) {
            return false;
        }

        $this->setHour($this->getMinute());
        $this->setMinute($this->getHour());

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_SWAP_HANDS_READ_AS_HOURS_WITH_PAST_TO.
     *
     * Not applied when time is 03:mm, 09:mm, 15:mm, or 21:mm.
     *
     * 1. Swap hands.
     * 2. Read minutes according to hour markings.
     * 3. Minutes past/to according to what half of the face the hand is on.
     *
     * @return bool
     */
    public function errorAnalogueSwapHandsReadAsHoursWithPastTo()
    {
        if (!in_array($this->getMinute(), [50]) && (
             in_array($this->getHour(), [3, 15, 9, 21]) ||
            !in_array($this->getHour(), [1, 13, 5, 17, 7, 19, 11, 23])
            )
        ) {
            return false;
        }

        $this->errorAnalogueSwapHands();
        $this->errorAnalogueMinutesReadAsHoursWithPastTo();

        return $this->setAnswerMode(__FUNCTION__);
    }

    /**
     * Generate answer ERROR_ANALOGUE_SWAP_HANDS_READ_AS_MINUTES_WITH_PAST_TO.
     *
     * @return bool
     */
    public function errorAnalogueSwapHandsReadAsMinutesWithPastTo()
    {
        $minutes = $this->to12hours($this->getHour());
        $hours = $this->toHours($this->getMinute());

        $this->setMinute(($hours <= 6) ? $minutes : 60 - $minutes);
        $this->setHour($hours);
        $this->deltaHours(-1);

        return $this->setAnswerMode(__FUNCTION__);
    }
}
