<?php

namespace AppBundle\RikaTika\TimeGenerator;

/**
 * TimeGeneratorEN.
 *
 * @author Olivier Parent <olivier.parent@arteveldehs.be>
 * @copyright Copyright © 2011-2015, Artevelde University College Ghent
 * @license http://www.clocklearning.org/LICENSE.txt
 */
class TimeGeneratorEN extends TimeGeneratorAbstract
{
    const ANTE_MERIDIEM = 'a.m.';
    const FORMAT_DIGITAL_MINUTES_01_09 = '%1$d O %2$d';
    const FORMAT_DIGITAL_MINUTES_10_59 = '%1$d %2$d';
    const FORMAT_DIGITAL_FULL_HOUR = '%1$d';
    const FORMAT_ANALOGUE_FULL_HOUR = '%1$d o’clock';
    const FORMAT_ANALOGUE_PAST = '%2$s past %1$d';
    const FORMAT_ANALOGUE_TO = '%2$s to %1$d';
    const HALF = 'half';
    const POST_MERIDIEM = 'p.m.';
    const QUARTER = 'a quarter';

    /**
     * Convert time in hours and minutes to a formatted text string.
     *
     * @param string $type
     *
     * @return string
     *
     * @throws \ErrorException
     */
    public function toText($type = self::TEXT_ANALOGUE)
    {
        $h = $this->getHour();
        $m = $this->getMinute();

        switch ($type) {
            case self::TEXT_DIGITAL:
                if (0 <= $h && $h < 24) {
                    $meridian = ($h < 12) ? self::ANTE_MERIDIEM : self::POST_MERIDIEM;

                    if (0 === $h) {
                        $h = 12;
                    } elseif (12 < $h) {
                        $h -= 12;
                    }
                } else {
                    throw new \ErrorException("Illegal value <strong>{$h}</strong> for hours in class <strong>".get_called_class().'</strong>');
                }

                if (0 === $m) {
                    $format = self::FORMAT_DIGITAL_FULL_HOUR;
                } elseif (1 <= $m && $m <= 9) {
                    $format = self::FORMAT_DIGITAL_MINUTES_01_09;
                } else {
                    $format = self::FORMAT_DIGITAL_MINUTES_10_59;
                }

                $format .= ' '.$meridian;
                break;
            case self::TEXT_ANALOGUE:
            default:
                if (0 <= $h && $h < 24) {
                    if (0 === $h) {
                        $h = 12;
                    } elseif (12 < $h) {
                        $h -= 12;
                    }
                } else {
                    throw new \ErrorException("Illegal value <strong>{$h}</strong> for hours in class <strong>".get_called_class().'</strong>');
                }

                if (0 === $m) {
                    $format = self::FORMAT_ANALOGUE_FULL_HOUR;
                } elseif (1 <= $m && $m <= 30) {
                    $format = self::FORMAT_ANALOGUE_PAST;
                } elseif (31 <= $m && $m <= 59) {
                    $format = self::FORMAT_ANALOGUE_TO;
                    if (12 === $h++) {
                        $h = 1;
                    }
                    $m = 60 - $m;
                } else {
                    throw new \ErrorException("Illegal value <strong>{$m}</strong> for minutes in class <strong>".get_called_class().'</strong>');
                }

                if (15 === $m) {
                    $m = self::QUARTER;
                }
                if (30 === $m) {
                    $m = self::HALF;
                }
                break;
        }

        return sprintf($format, $h, $m);
    }
}
