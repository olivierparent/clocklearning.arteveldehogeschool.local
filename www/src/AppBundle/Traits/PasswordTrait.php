<?php

namespace AppBundle\Traits;

use AppBundle\Entity\UserAbstract;

trait PasswordTrait
{
    /**
     * @param UserAbstract $user
     */
    public function hashPassword(UserAbstract $user)
    {
        $container = method_exists($this, 'getContainer') ? $this->getContainer() : $this;
        $passwordEncoder = $container->get('security.password_encoder');
        $encodedPassword = $passwordEncoder->encodePassword($user, $user->getPassword());
        $user->setPassword($encodedPassword);
    }
}