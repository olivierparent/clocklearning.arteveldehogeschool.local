Rika-Tika v3
============

Domeinnaam mapping
------------------

Gebruik [Artestead](https://github.com/olivierparent/homestead).

Open `Homestead.yaml`:
 
    $ artestead edit

voeg je de mapping toe:

```yaml
# Rika-Tika
# ---------
    - map: v1.clocklearning.arteveldehogeschool.local
      to: /home/vagrant/Code/clocklearning.arteveldehogeschool.local/v1/public

    - map: v2.clocklearning.arteveldehogeschool.local
      to: /home/vagrant/Code/clocklearning.arteveldehogeschool.local/v2/web
      type: symfony2
      
    - map: v3.clocklearning.arteveldehogeschool.local
      to: /home/vagrant/Code/clocklearning.arteveldehogeschool.local/v3/web
      type: symfony2
```

> **Opmerking**  
> Enkel de laatste (v3) is van toepassing op dit project.

Vagrant starten met de nieuwe provision scripts.

Mac OS X:

    $ artestead up --provision

Windows:

    $ vagrant up --provision

Project clonen
--------------

Meld je aan op de Virtuele machine


Mac OS X:

    $ artestead ssh

Windows:

    $ vagrant ssh

Maak een nieuwe map en cloon het project:

    vagrant@homestead$ mkdir -p ~/Code/clocklearning.arteveldehogeschool.local/
    vagrant@homestead$ cd ~/Code/clocklearning.arteveldehogeschool.local/
    vagrant@homestead$ git clone https://bitbucket.org/olivierparent/v3.clocklearning.arteveldehogeschool.local/
    vagrant@homestead$ cd v3/
    vagrant@homestead$ sudo composer self-update
    vagrant@homestead$ composer update
    vagrant@homestead$ console --version

Database
--------

> **OPGELET**  
> Als de Virtuele Machine op een Windows Guest geïnstalleerd is, moet je de mogelijk de MS-DOS line endings naar Unix line endings converteren voordat de scripts uitgevoerd kunnen worden. 
>
>      vagrant@homestead$ dos2unix **/.settings **/*.sh app/console

    vagrant@homestead$ cd Code/clocklearning.arteveldehogeschool.local/v3/
    vagrant@homestead$ database/init.sh
    vagrant@homestead$ console doctrine:database:create
    vagrant@homestead$ console doctrine:schema:create
    vagrant@homestead$ console doctrine:fixtures:load

Test de installatie
----------------

### HTTP

 - http://v3.clocklearning.arteveldehogeschool.local
 - http://v3.clocklearning.arteveldehogeschool.local/app_dev.php

### HTTPS

 - https://v3.clocklearning.arteveldehogeschool.local
 - https://v3.clocklearning.arteveldehogeschool.local/app_dev.php
